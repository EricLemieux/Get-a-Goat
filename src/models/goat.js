const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const GoatSchema = new mongoose.Schema({
  key: {
    type: String,
    trim: false,
  },
  width: {
    type: Number,
  },
  height: {
    type: Number,
  },
});

module.exports = GoatSchema;
