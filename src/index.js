const express = require('express');
const request = require('request');
const mongoose = require('mongoose');
const AWS = require('aws-sdk');
const goatSchema = require('./models/goat');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 8000;
const MONGODB_URI = process.env.MONGODB_URI || null;

const S3_BUCKET = process.env.S3_BUCKET || null;
const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID || null;
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY || null;

mongoose.Promise = global.Promise;

/**
 * Main program starting point.
 */
function main() {
  if (MONGODB_URI && AWS_ACCESS_KEY_ID && AWS_SECRET_ACCESS_KEY && S3_BUCKET) {
    // Connect to mongo.
    mongoose.connect(MONGODB_URI)
      .catch((err) => {
        console.log(`There was an error connecting to mongo:\n${err}`); // eslint-disable-line no-console
      });

    const Goat = mongoose.model('Goat', goatSchema, 'PlaceGoat');

    // Configure AWS credentials.
    AWS.config.update({
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY,
    });

    // Connect to S3.
    const S3 = new AWS.S3({
      region: 'us-east-2',
    });

    app.use(express.static('public'));

    app.get('/random', async (req, res) => {
      try {
        const data = await Goat.aggregate([{
          $sample: {
            size: 1,
          },
        }]).exec();
        if (!data || data.length < 1) {
          throw Error('No goats found');
        }

        const signedUrl = S3.getSignedUrl('getObject', {
          Bucket: S3_BUCKET,
          Key: data[0].key,
        });

        request(signedUrl).pipe(res);
      } catch (e) {
        console.log(`Error finding goat models:\n${e}`); // eslint-disable-line no-console
        res.redirect('/');
      }
    });

    app.listen(PORT, () => console.log(`Port: ${PORT}`)); // eslint-disable-line no-console
  } else {
    console.log('You are missing key environment variables, please update your .env file'); // eslint-disable-line no-console
  }
}
main();
