# Get a Goat

Simple website that returns a random photo of a goat.

## Usage

`npm run start`

## Environment variables

``` env
.env
----------
PORT=8000
MONGODB_URI=your-mongo-uri
AWS_ACCESS_KEY_ID=123
AWS_SECRET_ACCESS_KEY=123
S3_BUCKET=abc
```
